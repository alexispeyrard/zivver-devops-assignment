# We use a small image that allows us to run the nginx web server.
FROM nginx:alpine

COPY /WebApp/index.html /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]