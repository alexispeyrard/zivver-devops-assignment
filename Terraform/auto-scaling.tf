## Auto Scaling based on our ECS service configuration.
resource "aws_appautoscaling_target" "ecs_scaling" {
  max_capacity = 4
  min_capacity = 2
  resource_id = "service/${aws_ecs_cluster.zivver-ecs-cluster.name}/${aws_ecs_service.app_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
}

# If the memory usage goes above 75%, the app will scale up. When it comes back below, it will scale down.
resource "aws_appautoscaling_policy" "memory-scaling-policy" {
  name = "memory-scaling-policy"
  policy_type = "TargetTrackingScaling"
  resource_id = aws_appautoscaling_target.ecs_scaling.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_scaling.scalable_dimension
  service_namespace = aws_appautoscaling_target.ecs_scaling.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value = 75
  }
}

# If the CPU usage goes above 75%, the app will scale up. When it comes back below, it will scale down.
resource "aws_appautoscaling_policy" "cpu-scaling-policy" {
  name = "cpu-scaling-policy"
  policy_type = "TargetTrackingScaling"
  resource_id = aws_appautoscaling_target.ecs_scaling.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_scaling.scalable_dimension
  service_namespace = aws_appautoscaling_target.ecs_scaling.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }
    target_value = 75
  }
}