## Virtual Private Cloud.
resource "aws_vpc" "zivver-vpc" {
    cidr_block = "10.0.0.0/16"
    tags = {
        name = "zivver-vpc"
    }
}

## Subnets over different AZs for high-availability.
variable "sub-cidr" {
    default = ["10.0.1.0/24" , "10.0.2.0/24"]
}

variable "sub-az" {
    default = ["eu-west-2a" , "eu-west-2b"]
}

resource "aws_subnet" "zivver-subnet" {
    vpc_id = aws_vpc.zivver-vpc.id
    count = length(var.sub-cidr)
    cidr_block = var.sub-cidr[count.index]
    availability_zone = var.sub-az[count.index]
    map_public_ip_on_launch = true

    tags = {
        Name = "zivver-subnet-${count.index}"
    }
}

## Internet Gateway.
resource "aws_internet_gateway" "zivver-igw" {
    vpc_id = aws_vpc.zivver-vpc.id
    tags = {
        name = "zivver-igw"
    }
}

## Routing to link the internet gateway and the subnets.
resource "aws_route_table" "vpc-igw-route" {
    vpc_id = aws_vpc.zivver-vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.zivver-igw.id
    }
}

resource "aws_route_table_association" "subnet-route" {
    count = length(var.sub-cidr)
    subnet_id = aws_subnet.zivver-subnet[count.index].id
    route_table_id = aws_route_table.vpc-igw-route.id
}