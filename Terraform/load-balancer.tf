## Application Load Balancer will share and route the traffic towards our cluster. 
resource "aws_alb" "zivver-alb" {
  name = "zivver-alb"
  load_balancer_type = "application"
  subnets = aws_subnet.zivver-subnet[*].id
  security_groups = [aws_security_group.lb-sg.id]
}

resource "aws_lb_target_group" "lb-tg" {
  name = "lb-tg"
  port = 80
  protocol = "HTTP"
  target_type = "ip"
  vpc_id = aws_vpc.zivver-vpc.id 
}

resource "aws_lb_listener" "lb-listener" {
  load_balancer_arn = aws_alb.zivver-alb.arn
  port = "80"
  protocol = "HTTP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.lb-tg.arn
  }
}

## Security Group to control the traffic of our load balancer.
resource "aws_security_group" "lb-sg" {
    name = "lb-sg"
    vpc_id = aws_vpc.zivver-vpc.id

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}