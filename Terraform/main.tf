## Provider definition.
terraform {
    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = "5.25.0"
      }
    }
    backend "http" {}
}

provider "aws" {
    region = "eu-west-2"
}