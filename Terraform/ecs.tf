## Elastic Container Repository stores our webapp Docker image
resource "aws_ecr_repository" "zivver-ecr" {
    name = "zivver-webapp-repo"
    force_delete = true
}

## Elastic Container Service cluster will run our webapp.
resource "aws_ecs_cluster" "zivver-ecs-cluster" {
  name = "zivver-ecs-cluster"
}

## ECS task definition describes the desired state of every container.
resource "aws_ecs_task_definition" "app_task" {
  family = "launch-webapp-task"
  container_definitions = <<DEFINITION
  [
    {
      "name": "zivver-webapp-container",
      "image": "${aws_ecr_repository.zivver-ecr.repository_url}:latest",
      "portMappings": [
        {
          "containerPort": 80,
          "hostPort": 80
        }
      ],
      "memory": 512,
      "cpu": 256
    }
  ]
  DEFINITION

  requires_compatibilities = ["FARGATE"] ## We use Fargate over EC2 to ensure easy maintainability and lower costs.
  network_mode = "awsvpc"
  memory = 512
  cpu = 256
  execution_role_arn = aws_iam_role.ecsTaskRole.arn
  task_role_arn = aws_iam_role.ecsTaskRole.arn
}

## ECS service describes the desired state of the cluster.
resource "aws_ecs_service" "app_service" {
  name = "app-service"
  cluster = aws_ecs_cluster.zivver-ecs-cluster.id
  task_definition = aws_ecs_task_definition.app_task.arn
  launch_type = "FARGATE"
  desired_count = 2

  load_balancer {
    target_group_arn = aws_lb_target_group.lb-tg.arn
    container_name = "zivver-webapp-container"
    container_port = 80
  }

  network_configuration {
    subnets = aws_subnet.zivver-subnet[*].id
    assign_public_ip = true
    security_groups = [aws_security_group.ecs-service_sg.id]
  }
}

## Security Group only allowing connection to the containers from the load balancer.
resource "aws_security_group" "ecs-service_sg" {
    name = "ecs-service-sg"
    vpc_id = aws_vpc.zivver-vpc.id

    ingress {
        from_port = 0
        to_port   = 0
        protocol  = "-1"
        security_groups = [aws_security_group.lb-sg.id]
    }

    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}