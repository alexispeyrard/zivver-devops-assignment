# Zivver DevOps Technical Assignment - Peyrard Alexis

## Description
This repository's primary function is to assess my Cloud & DevOps skills through the automation of a web application deployment for the Zivver company.

In this repository you will find:
  - The HTML code of a static web page and a Dockerfile creating an image of this code.
  - The Terraform code to deploy an AWS infrastructure that will host and run the web application.
  - A Gitlab CI pipeline that can automatically run the two previous steps.

Please read this file thoroughly, as it gives indications regarding how to run the project in your own environment.



## Disclaimer
The code in this repository was written to provide a solution following specific requirements and specifications; hence it may not be suitable to use in your production environment.
Please make sure that you fully understand this code before making any changes to it and/or using it in your production environment.



## Prerequisites
This project can either be run through Gitlab using a CI/CD pipeline or manually from your favorite IDE.
Some prerequisites are common for both methods, while others are specific to the manual run.

Common:
  - 'AWS' account with administrator rights.
  - 'Gitlab' account.

Manual run:
  - 'AWS CLI' locally installed.
  - 'Hashicorp Terraform' locally installed.
  - 'Docker Engine' locally installed.
  - 'Git' locally installed.



## Gitlab CI pipeline run

### Setup your Gitlab environment
The pipeline works with variables that you must define on your Gitlab account:
  - On the project page, go to the left banner. Select 'Settings' > 'CI/CD'.
  - Expand the 'Variables' section, then click 'Add variable'.
  - You now need to create 7 key/value pairs with your own data. Here's how:

|KEY							 |VALUE                        |
|--------------------------------|-----------------------------|
|AWS_ACCESS_KEY_ID       		 |Being logged in to your AWS account, navigate to 'IAM' > 'Users' > [your user with admin rights] > 'Security Credentials' > 'Create access key'. Then select 'CLI', tick the checkbox and click 'Next'. Give a recognizable name to your key and click 'Create access key'. You now have access to the **access key**. You will also gain one-time access to the **secret access key**, which is the next variable to create.|
|AWS_SECRET_ACCESS_KEY			 |Follow the previous step to obtain your **secret acces key**.|
|AWS_ACCOUNT_ID       			 |Being logged in to your AWS account, click on your username on the top right to find your **account ID**. It is made of 12 numbers.|
|AWS_DEFAULT_REGION				 |**eu-west-2**|
|PROJECT_ID						 |On the Gitlab project page, at the top, under the project name, you will find the **project ID**. It is made of 8 numbers.|
|TF_ACCESS_TOKEN				 |Being logged in to your Gitlab account, navigate to 'Settings' > 'Access Tokens', and click 'Add new token'. Give a recognizable name to your token, select 'Owner' role, tick 'api' and click 'Create project access token'. Your can now copy your **access token**.|
|TF_USER						 |Being logged in to your Gitlab account, click on your profile picture on the top left to reveal your **username**. Copy it without the '@'.|

### Build the project
Once the setup is completed, you should be able to run the pipeline, as it will use the newly created variables:
  - On the project page, go to the left banner. Select 'Build' > 'Pipelines'.
  - On the top right, click on 'Run the pipeline'.
  - Leave the variable fields blank, and click 'Run pipeline'.

You will now see two jobs in progress : terraform_build and push_ecr.
1) terraform_build will build the whole AWS infrastructure to host the web application on your AWS account.
   Once this job is complete, it will print the application URL in the console. You can also find it in your AWS account under 'Load balancers' > 'zivver-alb' > 'DNS name'.
   Since the image hasn't been built yet, the link should display a 503 error code.
2) push_ecr will build a Docker image from the HTML file and push it onto the newly created Elastic Container Repository.

The execution of the jobs can take up to a few minutes. Once they're done, it can take an additional time for the infrastructure to refresh and fetch the image.
You can see if the containers have been properly deployed yet in your AWS account under 'ECS' > 'Clusters' > 'zivver-ecs-cluster' > 'Tasks' (2 should be in the running state).

Once the containers have been deployed using the image, refresh the application URL to access the page's content.

### Destroy the project
Once you're done with the project, you can remove all the AWS components created with the pipeline:
  - On the project page, go to the left banner. Select 'Build' > 'Pipelines'.
  - On the top right, click on 'Run the pipeline'.
  - On the variable field, set the following key/value pair:

  |KEY							 |VALUE                        |
  |------------------------------|-----------------------------|
  |PHASE                         |DESTROY                      |

  - Click 'Run pipeline'.

You will now see one job in progress: terraform_destroy. Wait until it completes to get your infrastructure cleaned.



## Manual installation and run
Before testing the manual run, please ensure that you have installed all the prerequisite tools on your machine.

### Setup your environment
  -Start by cloning the Gitlab repository to your local environment using git commands. If needed, please refer to the [official documentation](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).

  - Setup your AWS environment. In the console, type ```aws configure``` and fill in the different fields.
    To obtain your AWS Access Key ID and AWS Secret Acces Key, please have a look at [this table](#setup-your-gitlab-environment).
    Default region name: eu-west-2
    Default output format: json
    If needed, please refer to the [official documentation](https://docs.aws.amazon.com/cli/latest/userguide/welcome-examples.html).

### Build the Terraform infrastructure
In the console:
  - Navigate to /Terraform
  - Run the following commands:

  ⚠️ Replace all $... occurences by your data. If needed, please have a look at [this table](#setup-your-gitlab-environment).
  ```
  terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/default" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/default/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$PROJECT_ID/terraform/state/default/lock" \
    -backend-config="username=$TF_USER" \
    -backend-config="password=$TF_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
  ```
  ```
  terraform plan -out=tfplan
  ```
  ```
  terraform apply -auto-approve tfplan
  ```

Once the last command completes, it will print the application URL in the console. You can also find it in your AWS account under 'Load balancers' > 'zivver-alb' > 'DNS name'.
Since the image hasn't been built yet, the link should display a 503 error code. You will discover how to build the image in the next step.

### Build and push the Docker image
For this step, please ensure that Docker Engine is running on your machine.
In the console:
  - Navigate to /WebApp
  - Run the following commands:

  ⚠️ Replace all $... occurences by your data. If needed, please have a look at [this table](#setup-your-gitlab-environment).
  ```
  docker build -t $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/zivver-webapp-repo:latest .
  ```
  ```
  docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/zivver-webapp-repo:latest
  ```

These commands will build a Docker image from the HTML file and push it onto the newly created Elastic Container Repository.
It can take an additional time for the infrastructure to refresh and fetch the image.
You can see if the containers have been properly deployed yet in your AWS account under 'ECS' > 'Clusters' > 'zivver-ecs-cluster' > 'Tasks' (2 should be in the running state).

Once the containers have been deployed using the image, refresh the application URL to access the page's content.

### Destroy the project
Once you're done with the project, you can remove all the AWS components created with Terraform:
  - Navigate to /Terraform
  - Run the following commands:
  ```
  terraform plan -destroy -out=tfplan
  ```
  ```
  terraform apply -auto-approve tfplan
  ```
  Wait until this command completes to get your infrastructure cleaned.




## Contact
If you encounter any issues during the deployment of the project or if you have any questions, please reach out to me at alexis.peyrard@hotmail.com.


# To go further
This project is yet to be implemented in a production environment as we can still upgrade it.

## Improve the current design

Cost & performance optimization:
  - Both Terraform and gitlab-ci codes could be optimized to make them clearer and faster to run.
  - Within the ECS cluster, I decided to use Fargate over EC2 in order to lower the costs and facilitate the maintenability. However, we could face situations where we need a more personalized configuration or to handle heavier workloads, then we might need to migrate to a EC2 usage.
  - The auto-scaling has been implemented but not tested in high-traffic conditions. We could optimize its configuration to meet real life expectations.

Security:
  - This project is not intended to be used in a production environment, so the security configuration has been kept to a low level. In an actual production environment, we would need to add way more restrictions and granular access control. We should only open necessary ports, as well as granting users and role the least privileges.
  - The infrastructure lacks a backup system in case of disaster. The Terraform state is stored and crypted within Gitlab, but we don't have data backup for the AWS components. It would be a problem if we had a more evolved website. 



## Determine fault and issues

CI/CD:
  - We should add tests at different stages within the Gitlab pipeline to spot issues and alert the dev team.
  - Implement health-checks to ensure new changes didn't affect the infrastructure negatively.

Monitoring:
  - We should implement AWS CloudWatch to monitor AWS ressources, collect insights on the infrastructure' performances, collect log files and trigger alarms when detecting unwanted behaviors.
  - We could implement AWS Config to keep our ressources in a desired state and track any changes.